export default {
    port: process.env.APP_PORT || 8081,
    jenkinsToken: process.env.JENKINS_TOKEN || "Nothing"
}