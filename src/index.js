import express from "express";
import config from "./config";
import request from "request";

const app = express();

app.get("/health", (req, res) => {
    res.send({status: "UP"})
})

app.get("/jenkins", (req, res) => {
    let options = {
        url: "http://jenkins.quaff.harmelodic.com:8080/api/json?tree=jobs[name,url,color]",
        headers: {
            "Authorization": "Basic " + config.jenkinsToken
        }
    }

    request(options, (error, response, body) => {
        console.log(response.statusCode + " - " + response.statusMessage);
        if (error || (response.statusCode - 200) > 100) {
            res.status(500);
            res.send({
                error: response.statusMessage
            })
        } else {
            const responseBody = JSON.parse(body);

            res.send(responseBody.jobs.map(job => {
                switch (job.color) {
                    case "red":
                        job.color = "red";
                        break;
                    case "red_anime":
                        job.color = "red";
                        break;
                    case "yellow":
                        job.color = "yellow";
                        break;
                    case "yellow_anime":
                        job.color = "yellow";
                        break;
                    case "blue":
                        job.color = "green";
                        break;
                    case "blue_anime":
                        job.color = "green";
                        break;
                    case "grey":
                        job.color = "amber";
                        break;
                    case "grey_anime":
                        job.color = "amber";
                        break;
                    case "disabled":
                        job.color = "green";
                        break;
                    case "disabled_anime":
                        job.color = "green";
                        break;
                    case "aborted":
                        job.color = "amber";
                        break;
                    case "aborted_anime":
                        job.color = "amber";
                        break;
                    case "nobuilt":
                        job.color = "amber";
                        break;
                    case "nobuilt_anime":
                        job.color = "amber";
                        break;
                    default:
                        job.color = "red";
                        break;
                }
                return job;
            }));
        }
    })
});

app.listen(config.port, () => {
    console.log("Started application on port: " + config.port);
})