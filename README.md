# j-juicer

An Express, Node.js app to pull status information from Jenkins and expose it for the quaffacade to consume.

# JENKINS_TOKEN

Jenkins requires an API token to be sent on HTTP requests to authorise them.

In the J-Juicer, this API token needs to be sent on the ENVIRONMENT variable: $JENKINS_TOKEN.

To populate this in Kubernetes, the Pod reads a Kubernetes Secret and sets up an environment variable based on the value set in that Secret.

To setup the secret, connect to the cluster via `kubectl` and perform:

```
kubectl -n quaff-dev create secret generic jenkins --from-literal=token=<YOUR API TOKEN>
```

where `<YOUR API TOKEN>` is a base64 encoded string of `user_id:api_token` (fetched from `http://<JENKINS_INSTANCE>/user/admin/configure`)